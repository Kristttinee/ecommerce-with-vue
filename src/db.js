const data = [
    {
      name: "socks",
      variant: [
        {
          variantImage: require("./assets/vmSocks-blue.jpg"),
          variantId: 2234,
          variantColor: "blue",
          variantQuantity: 10,
        },
        {
          variantImage: require("./assets/vmSocks-green.jpg"),
          variantId: 2235,
          variantColor: "green",
          variantQuantity: 0,
        }
      ]
    },
    {
      name: "dress",
      variant: [
        {
          variantImage: require("./assets/dress1.jpg"),
          variantId: 2244,
          variantColor: "#A90B2B",
          variantQuantity: 10,
        },
        {
          variantImage: require("./assets/dress2.jpg"),
          variantId: 2245,
          variantColor: "blue", 
          variantQuantity: 10,
        }
      ]
    },
    {
      name: "shoes1",
      variant: [
        {
          variantImage: require("./assets/shoe1-1.jpg"),
          variantId: 2244,
          variantColor: "burlyWood",
          variantQuantity: 10,
        },
        {
          variantImage: require("./assets/shoe1-2.jpg"),
          variantId: 2245,
          variantColor: "black", 
          variantQuantity: 10,
        }
      ]
    },
    {
      name: "shoes2",
      variant: [
        {
          variantImage: require("./assets/shoe2-1.jpg"),
          variantId: 2244,
          variantColor: "burlyWood",
          variantQuantity: 10,
        },
        {
          variantImage: require("./assets/shoe2-2.jpg"),
          variantId: 2245,
          variantColor: "black", 
          variantQuantity: 10,
        }
      ]
    },
    {
      name: "shoes3",
      variant: [
        {
          variantImage: require("./assets/shoe3-1.jpg"),
          variantId: 2244,
          variantColor: "burlyWood",
          variantQuantity: 10,
        },
        {
          variantImage: require("./assets/shoe3-2.jpg"),
          variantId: 2245,
          variantColor: "black", 
          variantQuantity: 10,
        }
      ]
    },
] 

export default data



